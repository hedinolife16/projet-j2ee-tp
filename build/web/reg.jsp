<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Todo List App</title>
          <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta charset="UTF-8">
    </head>
    <body>
        <%@page import="java.sql.*"%>
        <%@page import="javax.sql.*"%>
        <%  
            String pseudo= request.getParameter("pseudo");
            String pwd= request.getParameter("pwd");
            Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Todolist", "root","");
            Statement st = con.createStatement();
            
            PreparedStatement ps = con.prepareStatement("SELECT pseudo FROM user WHERE pseudo=?");
            ps.setString(1,pseudo);
            ResultSet rs = ps.executeQuery();
            String confirm = "none";
            
            int i=0;
            if (!rs.next()) {
                i = st.executeUpdate("INSERT INTO `todolist`.`user` (`pseudo`, `pwd`) VALUES ('"+pseudo+"','"+pwd+"')");
                confirm = "Compte créé";   
            }
            else{
                confirm = "Compte existe déja";
            }

        %>
        
        <div id="app">
        <v-app dark>
        <div>
            <v-toolbar>
                <v-spacer></v-spacer>
                <v-toolbar-items>
                    <v-btn flat href="./index.html">Sign in</v-btn>
                    <v-btn flat href="./login.html">Login</v-btn>
                    <v-btn flat color="red darken-3" disabled>Logout</v-btn>
                </v-toolbar-items>
            </v-toolbar>
            <form method="post" action="reg.jsp">
                <v-flex style="margin: 100px" outline>
                    <h2 style="margin: 20px; margin-left: -60px ;color: #26A69A">Créer un compte:</h2>
                    <v-flex xs2>
                        <v-text-field required label="Votre Pseudo" outline color="#26A69A" name="pseudo"></v-text-field>
                        <v-text-field type="password" required label="Votre Mot de passe" outline color="#26A69A" name="pwd"></v-text-field>
                    </v-flex>
                    <v-btn type="submit" color="#26A69A" outline flat>CREER LE COMPTE</v-btn>
                    <h4 style="margin-top: 20px; color:#9FA8DA"><% out.print(confirm); %></h4>
                </v-flex>   
            </form>
            <v-footer class="pa-3">
                <v-spacer></v-spacer>
                <div>&copy; {{ new Date().getFullYear() }} Hedi MLIKA</div>
            </v-footer>
        </div>
    </v-app>
  </div>
  
  <script src="./main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
  <script>
    new Vue({ el: '#app' })
  </script>
    </body>
</html>
