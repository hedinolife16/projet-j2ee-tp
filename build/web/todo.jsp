<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Créer un Todo</title>
          <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta charset="UTF-8">
    </head>
    <body>
        <%@page import="java.sql.*"%>
        <%@page import="javax.sql.*"%>
        <%@page import="java.util.*"%>
        
        <%  
            String confirm="";
            try{
                String pseudo = session.getAttribute("pseudo").toString();
                String titre= request.getParameter("titre");
                String message= request.getParameter("message");
                
                Class.forName("com.mysql.jdbc.Driver");
                java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Todolist", "root","");
                Statement st = con.createStatement();
                
                PreparedStatement ps = con.prepareStatement("SELECT titre FROM listtodo WHERE titre=?");
                ps.setString(1,titre);
                ResultSet rs = ps.executeQuery();
                
                int i=0;
                
                if (!rs.next()) {
                    i = st.executeUpdate("INSERT INTO `todolist`.`listtodo` (`titre`, `pseudo`, `message`, date) VALUES ('"+titre+"', '"+pseudo+"', '"+message+"','2018-12-13');");   
                    confirm="Rappel crée";
                }
                else{
                    confirm="Erreur, Essayez avec un autre titre";
                }
                
                
            }catch(Exception e){
                session.setAttribute("msg", "Please Log in first");
                response.sendRedirect("index.html");
            }
        %>
        
        <div id="app">
        <v-app dark>
        <div>
            <form method="post" action="logout.jsp">
            <v-toolbar>
                <v-spacer></v-spacer>
                <v-toolbar-items> 
                    <v-btn flat href="./todo.jsp" disabled>TODO List</v-btn>
                    <v-btn flat href="./index.html" disabled>Sign in</v-btn>
                    <v-btn flat href="./login.html" disabled>Login</v-btn>
                    <v-btn flat color="red darken-3" type="submit">Logout</v-btn>
                </v-toolbar-items>
            </v-toolbar>
            </form>
            
            <form method="post" action="todo.jsp">
                <v-flex style="margin: 100px" outline>
                    <h2 style="margin: 20px; margin-left: -60px ;color: #D81B60">Créer un Rappel:</h2>
                    <v-flex xs4>
                        <v-text-field required label="Titre / Activité" outline color="#D81B60" name="titre"></v-text-field>
                        <v-textarea required label="Description" outline color="#D81B60" name="message"></v-textarea>
                    </v-flex>
                    <v-btn type="submit" color="#D81B60" outline flat>CREER MON RAPPEL</v-btn>
                    <h4 style="margin-top: 20px; color:#D81B60"><% out.print(confirm); %></h4>
                </v-flex>   
            </form>
                
            <form method="post" action="todoview.jsp">
                <v-btn type="submit" color="#D81B60" round>VOIR MES RAPPELS</v-btn>
            </form>
            <form method="post" action="todoviewall.jsp">
                <v-btn type="submit" color="#D81B60" outline round>VOIR TOUS LES RAPPELS</v-btn>
            </form>
            <v-footer class="pa-3">
                <v-spacer></v-spacer>
                <div>&copy; {{ new Date().getFullYear() }} Hedi MLIKA</div>
            </v-footer>
        </div>
    </v-app>
  </div>
  
  <script src="./main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
  <script>
    new Vue({ el: '#app' })
  </script>
    </body>
</html>
