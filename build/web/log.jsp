<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenue</title>
          <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta charset="UTF-8">
    </head>
    <body>
        <%@page import="java.sql.*"%>
        <%@page import="javax.sql.*"%>
        <% 
            String pseudo= request.getParameter("pseudo");
            session.setAttribute("pseudo", pseudo);
            String pwd= request.getParameter("pwd");
            Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Todolist", "root","");
            Statement st = con.createStatement();
            
            PreparedStatement ps = con.prepareStatement("SELECT * FROM user WHERE pseudo='"+pseudo+"'");;
            ResultSet rs = ps.executeQuery();
            
            String confirm = "";
            
            if(!rs.next()){
                confirm = "Pseudo Invalide";
                out.println(confirm);
            }
            else{
                if(!rs.getString(2).equals(pwd)){
                    response.sendRedirect("index.html");
                    confirm = "Mot de passe Invalide";
                    out.println(confirm);
                }
                else{
                    session.setAttribute("sid", pseudo);
                    confirm = "Bienvenue "+ session.getAttribute("pseudo").toString();   
        %>
        
        <div id="app">
        <v-app dark>
        <div>
            <form method="post" action="logout.jsp">
            <v-toolbar>
                <v-spacer></v-spacer>
                <v-toolbar-items>
                    <v-btn flat href="./todo.html">TODO List</v-btn>
                    <v-btn flat href="./index.html" disabled>Sign in</v-btn>
                    <v-btn flat href="./login.html" disabled>Login</v-btn>
                    <v-btn flat color="red darken-3" type="submit">Logout</v-btn>
                </v-toolbar-items>
            </v-toolbar>
            </form> 
            <h4 style="margin-top: 20px; color:#9FA8DA"><% out.print(confirm); %></h4>
                
            <v-footer class="pa-3">
                <v-spacer></v-spacer>
                <div>&copy; {{ new Date().getFullYear() }} Hedi MLIKA</div>
            </v-footer>
        </div>
    </v-app>
  </div>
  
  <script src="./main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
  <script>
    new Vue({ el: '#app' })
  </script>
 <% } } %>
    </body>
</html>
