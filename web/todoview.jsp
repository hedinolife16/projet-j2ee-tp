<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mes TODOs</title>
          <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
        <link href="./main.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta charset="UTF-8">
    </head>
    <body>
        <%@page import="java.sql.*"%>
        <%@page import="javax.sql.*"%>
        
        <div id="app">
        <v-app dark>
            
            <v-layout align-center justify-center style="margin: 100px" light>
           <form method="post">

            <table>
                <tr>
                    <th style="font-size: 30px">Titre</th>
                    <th style="font-size: 30px">Pseudo</th>
                    <th style="font-size: 30px">Message</th>
                    <th style="font-size: 30px">Date</th>
                </tr>
                <%
                try
                    {
                    Class.forName("com.mysql.jdbc.Driver");
                    java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Todolist", "root","");
                    String pseudo= session.getAttribute("pseudo").toString();
                    String query="select * from listtodo WHERE pseudo='"+pseudo+"'";
                    Statement stmt=con.createStatement();
                    ResultSet rs=stmt.executeQuery(query);
                    while(rs.next())
                    {

                    %>
                    <tr>
                        <td style="color:white; font-size: 17px"><%=rs.getString(1) %></td>
                        <td style="color:white; font-size: 17px"><%=rs.getString(2) %></td>
                        <td style="color:white; font-size: 17px"><%=rs.getString(3) %></td>
                        <td style="color:white; font-size: 17px"><%=rs.getDate(4) %></td>
                    </tr>
                     <%
                    }
                %>
                </table>
                <%
                rs.close();
                stmt.close();
                con.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            %>
        </form>
        </v-layout>
        <v-btn type="submit" color="#D81B60" round href="./todo.html">CREER UN NOUVEAU RAPPEL</v-btn>
                    <v-footer class="pa-3">
                <v-spacer></v-spacer>
                <div>&copy; {{ new Date().getFullYear() }} Hedi MLIKA</div>
            </v-footer>
            </v-app>
  </div>
          <script src="./main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
  <script>
    new Vue({ el: '#app' })
  </script>
    </body>
</html>
